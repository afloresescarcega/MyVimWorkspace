If you want to be cool and want to remove your previous vim setup,
or start from scratch with mine do this.

```bash
mkdir -p ~/.vim 
cd ~/.vim
git clone --recurse-submodules -j8 https://gitlab.com/afloresescarcega/MyVimWorkspace/ .
ln vimrc ~/.vimrc
```

After doing that command,
open up vim in Normal Mode and enter 
```
:PluginInstall
```