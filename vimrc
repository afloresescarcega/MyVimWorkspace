 "Theme
syntax on
syntax enable 
colorscheme monokai


:imap jj <ESC>
:imap JJ <ESC>
:imap kk <ESC>
:imap KK <ESC>

" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-Tab> <C-d>

" disable auto comment
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

filetype plugin indent on
"show existing tab with 4 spaces width
:set tabstop=4
" when indenting with >, insert 4 spaces
:set shiftwidth=4
" ON pressing tab, insert 4 spaces
" set expandtab

"Turn on autoindent
:set autoindent

" Turn on smartindent
:set smartindent

" label number lines
:set number

" Turn on syntax highlighting
" syntax on

" " Add auto complete brackets
" inoremap { {<CR>}<Esc>ko


" " auto paren
" :inoremap ( ()<Esc>i

" diables auto comment
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"

" For Pathogen
" execute pathogen#infect()

" Non default recommended settings from Syntastic
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0

" changing bind of ctrl n to ctrl space
if has("gui_running")
    " C-Space seems to work under gVim on both Linux and win32
    inoremap <C-Space> <C-n>
else " no gui
  if has("unix")
    inoremap <Nul> <C-n>
  else
  " I have no idea of the name of Ctrl-Space elsewhere
  endif
endif

" ctrl n now toggles between relative line numbers and absolute
function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber 
  else
    set relativenumber
  endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>


"
"
"
"
" vundle bs
"
"
"
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
			

" Ack : better search than vim's grep
Plugin 'mileszs/ack.vim'
" NerdCommenter
Plugin 'scrooloose/nerdcommenter'

Plugin 'alvan/vim-closetag'
Plugin 'Yggdroot/indentLine'
" Plugin 'udalov/kotlin-vim'
" Plugin 'vim-syntastic/syntastic'
" Adds coloring to parenthesis
	
" Plugin 'kien/rainbow_parentheses.vim' 
	" Defunt
Plugin 'eapache/rainbow_parentheses.vim'

"Auto close pairs
Plugin 'jiangmiao/auto-pairs'


" YankRing I don't know how to use it and will disable for now
" Plugin 'vim-scripts/YankRing.vim'

" Neomake a async 
Plugin 'neomake/neomake'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


" Hopefully tabs in vim
autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window %")

" check one time after 4s of inactivity in normal mode
set autoread                                                                                                                                                                                    
au CursorHold * checktime 

" for mouse/trackpad scrolling
set mouse=a

" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-Tab> <C-d>

" highlight row
:set cursorline

" hightlight column
:set cursorcolumn

set tabstop=4

" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
"  match OverLength /\%81v.\+/

" Rainbow Parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Set leader to \
:let mapleader = ','

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

" mapping for yankring display
nnoremap <silent> <F11> :YRShow<CR>

" move to the end of the inside of the most inner set of paren
noremap ]] /]<CR>


" move to the end of the inside of the most inner set of curly brace 
noremap }} /}<CR>


" syntastic pintos stuff
" let g:syntastic_c_config_file = '~/Documents/2018Spring/CS439/cs439-pintos/.syntastic_c_config'



" move to the end of the inside of the most inner set of curly brace 
noremap }} /}<CR>


" syntastic pintos stuff
" let g:syntastic_c_config_file = '~/Documents/2018Spring/CS439/cs439-pintos/.syntastic_c_config'




" move to the end of the inside of the most inner set of curly brace 
noremap }} /}<CR>


" syntastic pintos stuff
" let g:syntastic_c_config_file = '~/Documents/2018Spring/CS439/cs439-pintos/.syntastic_c_config'
" let g:syntastic_c_check_header = 1

" Jump to a function from where it is called
" nmap gx yiw/^\(sub\<Bar>function\)\s\+<C-R>"<CR>

call neomake#configure#automake('nrwi', 500)







